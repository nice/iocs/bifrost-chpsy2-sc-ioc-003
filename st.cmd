#!/usr/bin/env iocsh.bash

require(mrfioc2)
require(evr_seq_calc)


epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

epicsEnvSet("IOC",          "bifrost-chpsy2-sc-ioc-003")
epicsEnvSet("SYS",          "BIFROST-ChpSy2:")
epicsEnvSet("EVR",          "EVR-001")
epicsEnvSet("DEV",          "Ctrl-$(EVR)")
epicsEnvSet("SYSPV",        "$(SYS)$(DEV)")
epicsEnvSet("PCIID",        "5:0.0")
epicsEnvSet("CHOP_SYS",     "$(SYS)")
epicsEnvSet("CHOP_EVR",     "$(DEV):")
epicsEnvSet("CHOP_CHIC",    "Chop-CHIC-001:")
epicsEnvSet("CHOP_DRV1",    "Chop-BWC-101:")
epicsEnvSet("CHOP_DRV2",    "Chop-BWC-102:")
epicsEnvSet("CHOP_DRV3",    "Chop-NA-101:")
epicsEnvSet("CHOP_DRV4",    "Chop-NA-201:")
epicsEnvSet("BASEEVTNO",    "25")
epicsEnvSet("MRF_HW_DB",    "evr-pcie-300dc-univ.db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

iocshLoad("$(mrfioc2_DIR)/evr.iocsh", "EVRDB=$(MRF_HW_DB), P=$(SYSPV), OBJ=$(EVR), PCIID=$(PCIID)")

iocshLoad("$(mrfioc2_DIR)/evrevt.iocsh", "P=$(SYSPV), OBJ=$(EVR)")

dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":00-", EVR=$(EVR), CODE=21, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":01-", EVR=$(EVR), CODE=22, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":02-", EVR=$(EVR), CODE=23, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":03-", EVR=$(EVR), CODE=24, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")


# Load the sequencer configuration script
iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc.iocsh", "P=$(CHOP_SYS), R1=$(CHOP_DRV1), R2=$(CHOP_DRV2), R3=$(CHOP_DRV3), R4=$(CHOP_DRV4), EVR=$(CHOP_EVR), BASEEVTNO=$(BASEEVTNO)")


iocInit()

iocshLoad("$(mrfioc2_DIR)/evr.r.iocsh", "P=$(SYSPV), OBJ=$(EVR)")

######### INPUTS #########
# Trig-Ext-Sel changed from "Off" to "Edge", Code-Ext-SP changed from 0 to 10
dbpf $(SYSPV):UnivIn-0-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-0-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB00-Src-SP 61
dbpf $(SYSPV):UnivIn-0-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-0-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-0-Code-Ext-SP 21
dbpf $(SYSPV):UnivIn-0-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-1-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-1-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB01-Src-SP 61
dbpf $(SYSPV):UnivIn-1-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-1-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-1-Code-Ext-SP 22
dbpf $(SYSPV):UnivIn-1-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-2-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-2-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB02-Src-SP 61
dbpf $(SYSPV):UnivIn-2-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-2-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-2-Code-Ext-SP 23
dbpf $(SYSPV):UnivIn-2-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-3-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-3-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB03-Src-SP 61
dbpf $(SYSPV):UnivIn-3-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-3-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-3-Code-Ext-SP 24
dbpf $(SYSPV):UnivIn-3-Code-Back-SP 0

dbpf $(SYSPV):EvtA-SP.OUT "@OBJ=$(EVR),Code=14" 
dbpf $(SYSPV):EvtA-SP.VAL 14 
dbpf $(SYSPV):EvtB-SP.OUT "@OBJ=$(EVR),Code=21" 
dbpf $(SYSPV):EvtB-SP.VAL 21 
dbpf $(SYSPV):EvtC-SP.OUT "@OBJ=$(EVR),Code=22" 
dbpf $(SYSPV):EvtC-SP.VAL 22
dbpf $(SYSPV):EvtD-SP.OUT "@OBJ=$(EVR),Code=23"
dbpf $(SYSPV):EvtD-SP.VAL 23
dbpf $(SYSPV):EvtE-SP.OUT "@OBJ=$(EVR),Code=24"
dbpf $(SYSPV):EvtE-SP.VAL 24
dbpf $(SYSPV):EvtF-SP.OUT "@OBJ=$(EVR),Code=25"
dbpf $(SYSPV):EvtF-SP.VAL 25
dbpf $(SYSPV):EvtG-SP.OUT "@OBJ=$(EVR),Code=26"
dbpf $(SYSPV):EvtG-SP.VAL 26
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=$(EVR),Code=27"
dbpf $(SYSPV):EvtH-SP.VAL 27
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=$(EVR),Code=28"
dbpf $(SYSPV):EvtH-SP.VAL 28
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=$(EVR),Code=29"
dbpf $(SYSPV):EvtH-SP.VAL 29
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=$(EVR),Code=30"
dbpf $(SYSPV):EvtH-SP.VAL 30



######### OUTPUTS #########
dbpf $(SYSPV):DlyGen-0-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-0-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-0-Evt-Trig0-SP 14

dbpf $(SYSPV):DlyGen-1-Evt-Trig0-SP 14
dbpf $(SYSPV):DlyGen-1-Width-SP 2860 #1ms
dbpf $(SYSPV):DlyGen-1-Delay-SP 0 #0ms

#Set up delay generator 2 to trigger on event 25 and output 4
dbpf $(SYSPV):DlyGen-2-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-2-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-2-Evt-Trig0-SP 25
dbpf $(SYSPV):Out-RB04-Src-SP 2

dbpf $(SYSPV):DlyGen-3-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-3-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-3-Evt-Trig0-SP 26
dbpf $(SYSPV):Out-RB05-Src-SP 3

dbpf $(SYSPV):DlyGen-4-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-4-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-4-Evt-Trig0-SP 27
dbpf $(SYSPV):Out-RB06-Src-SP 4

dbpf $(SYSPV):DlyGen-5-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-5-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-5-Evt-Trig0-SP 28
dbpf $(SYSPV):Out-RB07-Src-SP 5


######## Sequencer #########
dbpf $(SYSPV):EndEvtTicks 4

# Load sequencer setup
dbpf $(SYSPV):SoftSeq-0-Load-Cmd 1

# Enable sequencer
dbpf $(SYSPV):SoftSeq-0-Enable-Cmd 1

# Select run mode, "Single" needs a new Enable-Cmd every time, "Normal" needs Enable-Cmd once
dbpf $(SYSPV):SoftSeq-0-RunMode-Sel "Normal"


# Use ticks or microseconds
dbpf $(SYSPV):SoftSeq-0-TsResolution-Sel "Ticks"

# Select trigger source for soft seq 0, trigger source 0, delay gen 0
dbpf $(SYSPV):SoftSeq-0-TrigSrc-0-Sel 0

# Commit all the settings for the sequnce
# commit-cmd by evrseq!!! 
dbpf $(SYSPV):SoftSeq-0-Commit-Cmd "1"
